import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';



import {
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatMenuModule,
    MatGridListModule,
    MatToolbarModule,
    MatButtonModule,
    MatStepperModule,
    MatDividerModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule
} from '@angular/material';


@NgModule({
	imports: [
		CommonModule,
        FormsModule,
        MatMenuModule,
        MatGridListModule,
        MatToolbarModule,
        MatButtonModule,
        MatStepperModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTabsModule,
        FormsModule,
        ReactiveFormsModule,
		RouterModule.forChild([
			{
				path: '',
                component: HomeComponent
			}
		])
	],
    declarations: [HomeComponent],
	providers : [
	]

})
export class HomeComponentModule { }
