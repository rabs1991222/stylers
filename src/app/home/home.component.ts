import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


interface categoryCard {
	title: string;
	text: string;
	img: string;
}
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	title = 'styler';

	isLinear = false;
	firstFormGroup: FormGroup;
	secondFormGroup: FormGroup;

	categoryCardArray: Array<categoryCard> = [
		{
			title: 'PELO ',
			text: "Nuestros Stylers crearán para vos el look que quieras. Ondas, brushing, corte, peinados, tratamientos capilares y más.",
			img: "http://www.stylers.me/wp-content/uploads/2018/07/pexels-photo-1103625-600x399.jpeg"
		},
		{
			title: 'MAQUILLAJE ',
			text: "Si querés lucir espectacular, un Styler correrá hasta vos y te maquillará de la manera que gustes. También, podrá darte clases de maquillaje personalizadas.",
			img: "http://www.stylers.me/wp-content/uploads/2018/06/red-woman-girl-brown-600x399.jpg"
		},
		{
			title: 'MANICURE ',
			text: "Nail Art, manicure, pedicure, uñas de gel y más. Una experiencia de lujo y con el máximo cuidado en la higiene.",
			img: "http://www.stylers.me/wp-content/uploads/2018/06/demonstration-hair-model-show-52499-600x399.jpeg"
		},
		{
			title: 'CASAMIENTOS ',
			text: "Todo que necesites para el gran día (y cualquier tipo de evento). Vos y tus acompañantes serán recibirán una atención integral y 100% personalizada. ",
			img: "http://www.stylers.me/wp-content/uploads/2018/06/pexels-photo-361755-600x399.jpeg"
		},
	]

	constructor(private _formBuilder: FormBuilder) {

	}

	ngOnInit() {
		this.firstFormGroup = this._formBuilder.group({
			firstCtrl: ['', Validators.required]
		});
		this.secondFormGroup = this._formBuilder.group({
			secondCtrl: ['', Validators.required]
		});
	}

}
