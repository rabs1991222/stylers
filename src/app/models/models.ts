export class PortalCategory {
	Id: number;
	Name: string;
	ParentId: number;
	Title: string;
	baseUrl: string;
	Categories: Array<PortalCategory>;
	menuUrl: string;
	items: Array<PortalCategory>;
    parsedCustomFields : Object;
	CustomFields : Array<any> = [];
	Url : string;
	constructor(){}
}

export interface Users {
    id: string;
	name: string;
	lastname: string;
	age: number;
	gender: string;
    email: string;
}

export interface UserLogin {
	email : string,
	password : any
}

export interface UserCreate {
	name : string;
	lastname: string;
	age: number;
	gender: string;
	email: string;
	password: any;
	img?:string
}

export class User {
	uid : string;
}

export class wauError {
	code: string;
	message: string;
}