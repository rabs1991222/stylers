import { config } from "../app.config";
import { Users, UserLogin, UserCreate } from "../models/models";
import { first, mergeMap, switchMap, tap, finalize } from 'rxjs/operators';
import { from, BehaviorSubject } from 'rxjs';
import { AngularFireStorage } from 'angularfire2/storage';

import {
	AngularFirestoreDocument,
	AngularFirestore,
	AngularFirestoreCollection
} from "angularfire2/firestore";

import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';

interface userSession {
	user: firebase.User;
	logged: boolean;
}

interface AllUserTypes {
	user: UserCreate;
	fireUser: firebase.User;
}

@Injectable()
export class AuthService {
	user: Observable<firebase.User>;
	f: any;
	/** instancio variables para firestore **/
	authenticationState = new BehaviorSubject(false);
	users: AngularFirestoreCollection<Users>;
	private taskDoc: AngularFirestoreDocument<Users>;
	/** instancio variables para firestore **/

	constructor(
		private firebaseAuth: AngularFireAuth,
		private db: AngularFirestore,
		public angularFireStorage: AngularFireStorage
	) {
		this.f = this.firebaseAuth;
		// this.platform.ready().then(
		// 	() => {
		this.user = firebaseAuth.authState;
		this.users = db.collection<Users>(config.collection_endpoint);
		this.getCurrentUser().then(
			data => {
				this.authenticationState.next(data.logged);
			}
		)
		// 	}
		// )
	}


	//addTask
	addUserFireStorage(id, user) {
		return this.db.collection("users").doc(id).set(user);
	}

	sendMailVerification(user) {
		return user.sendEmailVerification();
	}

	verifyData(actionCode: string, type?: string): Promise<string> {
		let firebaseAuth = this.firebaseAuth;
		let returnedData;
		return new Promise((resolve, reject) => {
			firebaseAuth.auth.checkActionCode(actionCode).then(function (info) {
				if (type == 'restoreEmail') {
					returnedData = info['data']['email'];
				}

				firebaseAuth.auth.applyActionCode(actionCode).then(() => {
					resolve(returnedData);
				}).catch((error) => {
					reject(error);
				}
				)
			}).catch(err => reject(err));
		});
	}

	async restoreEmail(actionCode: string) {
		let firebaseAuth = this.firebaseAuth;
		try {
			let email = await this.verifyData(actionCode, 'restoreEmail');
			try {
				await firebaseAuth.auth.sendPasswordResetEmail(email);
				console.log('lorreo de reinicio de contrasena');
			} catch (error) {
				console.log(error);
			}
		} catch (error) {
			console.log(error);
		}
	}

	async restorePassword(actionCode, newPassword) {
		let firebaseAuth = this.firebaseAuth.auth;
		try {
			await firebaseAuth.verifyPasswordResetCode(actionCode);
			try {
				await firebaseAuth.confirmPasswordReset(actionCode, newPassword);
			} catch (error) {
				console.error(error);
			}
		} catch (error) {
			console.error(error);
		}
	}

	async getUser(id) {
		let userRef
		try {
			userRef = await this.db.collection("users").doc(id).get().toPromise();
			console.log(userRef.data());
			return userRef.data();
		} catch (error) {
			return error;
		}
	}

	async updateFireUser(
		fireUser: firebase.User,
		displayName: string,
		photoURL?: string) {
		let photo = photoURL || '';
		try {
			fireUser.updateProfile(
				{
					displayName: displayName,
					photoURL: photo
				}
			)
		} catch (error) {
			console.log(error);
			return error;
		}

	}

	async updateUser(user: UserCreate, firebaseData: firebase.User): Promise<AllUserTypes> {
		try {
			await this.updateFireUser(firebaseData, user.name);
			try {
				await this.addUserFireStorage(firebaseData.uid, user);
				return { fireUser: firebaseData, user: user };
			} catch (error) {
				return error;
			}
		} catch (error) {
			console.error(error);
		}
	}

	async signup(user: UserCreate) {
		return new Promise((resolve, reject) => {
			this.firebaseAuth.auth.createUserWithEmailAndPassword(user.email, user.password).then(async value => {
				if (typeof user.img == 'object') {
					try {
						let savedImg = await this.saveFile(user.img);
						user.img = savedImg;
					} catch (error) {
						reject(error);
					}
				}
				this.updateUser(user, value.user).then(user => {
					this.authenticationState.next(true);
					resolve(user);
				}).catch(error => {
					reject(error)
				})
			}).catch(err => {
				this.authenticationState.next(false);
				console.log('Something went wrong:', err.message);
				reject(err);
			});
		});
	}

	sendRestorePasswordRequest(email: string) {
		return this.firebaseAuth.auth.sendPasswordResetEmail(email);
	}

	isAuthenticated() {
		// console.log(this.firebaseAuth.auth.currentUser);
		return this.firebaseAuth.user;
		// return this.authenticationState.value;
	}

	async login(email: string, password: string) {
		let logged;
		let provider = await this.verifyProvider(email);
		if (provider[0] == 'password') {
			try {
				logged = await this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
				this.authenticationState.next(true);
				console.log(logged)
			} catch (error) {
				logged = error;
				this.authenticationState.next(false);
				console.log(logged)
			}
		} else {
			logged = { user: false, code: "auth/account-exists-with-different-credential" }
		}
		return logged;
	}

	async logout() {
		let logout = await this.firebaseAuth.auth.signOut();
		this.authenticationState.next(false);
		return logout;
	}

	async getCurrentUser(): Promise<userSession> {
		let user = await this.firebaseAuth.authState.pipe(first()).toPromise();
		let userObj: userSession;
		if (user != null) {
			userObj = { user: user, logged: true };
		} else {
			userObj = { user: null, logged: false }
		}

		return userObj;
	}

	async signInProvider(provider: string) {
		return new Promise(async (resolve, reject) => {
			let providerType;
			let result;
			let exist;
			if (provider == 'facebook') {
				providerType = new firebase.auth.FacebookAuthProvider();
			};
			try {
				result = await this.firebaseAuth.auth.signInWithPopup(providerType);
				try {
					exist = await this.checkFireStoregaUserInDataBase(result.user.uid);
					if (!exist) {
						let newUser = {
							// accesssToken: result.credential.accessToken,
							uid: result.user.uid,
							name: result.user.displayName,
							email: result.user.email,
							emailVerified: result.user.emailVerified,
							photoURL: result.user.photoURL,
							provider: result.credential.providerId,
							gender: 'other'
						}
						try {
							await this.addUserFireStorage(newUser.uid, newUser);
						} catch (error) {
							reject(error);
						}
					}
					this.authenticationState.next(true);
					console.log(this.authenticationState.value);
					// debugger;
					resolve();
				} catch (error) {
					console.log(error);
				}
			} catch (error) {
				reject(error);
			}
		});
	}

	async verifyProvider(email: string) {
		return this.firebaseAuth.auth.fetchProvidersForEmail(email);
	}

	async checkFireStoregaUserInDataBase(id): Promise<boolean> {
		let exist;
		try {
			exist = await this.getUser(id);
			if (exist) {
				exist = true;
			} else {
				exist = false;
			}
			return exist;
		} catch (error) {
			console.log(error);
			return error
		}

	}

	async saveFile(file) {


		let path = `/uploads/${file.name}`;
		const ref = this.angularFireStorage.ref(path);
		try {
			await this.angularFireStorage.upload(path, file);
			let url = await ref.getDownloadURL().toPromise();
			return url;
		} catch (error) {
			return error;
		}
		/*  PORCENTAJE DE CARGA */
		// task.percentageChanges().subscribe(
		// 	data=>{
		// 		console.log(data);
		// 	}
		// )
		/*  PORCENTAJE DE CARGA */

		// return this.angularFireStorage.upload(path, file);
	}
}