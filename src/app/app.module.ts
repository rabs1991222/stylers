import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
	MatDatepickerModule
} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.modules';

//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FireBaseConfig } from './firebase-config';
import { AuthService, } from './services/auth.service';

import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';


export const firebaseConfig = {
	apiKey: "AIzaSyAiISJCTfI6eavx96a-QtTHFlOtBtJTEK8",
	authDomain: "stylers-c778e.firebaseapp.com",
	databaseURL: "https://stylers-c778e.firebaseio.com",
	projectId: "stylers-c778e",
	storageBucket: "stylers-c778e.appspot.com",
	messagingSenderId: "294962193265"
};

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		RouterModule.forRoot([]),
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFirestoreModule,
		AngularFireDatabaseModule,
    	AngularFireAuthModule       
	],
	providers: [
		MatDatepickerModule,
		AuthService,
		AngularFireDatabase,
		AngularFirestore,
		AngularFireStorage,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
