import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';


// import { AuthPage } from './auth.page';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from  './signup/signup.component'
// import { ProfileComponent } from './profile/profile.component';

// import { AuthGuardService } from "../auth/auth-guard.service";
// import { LoggedGuardService } from "../auth/logged-guard.service";


// import { LostPaswrodComponent } from './lost-paswrod/lost-paswrod.component';
// import { EditUserComponent } from './edit-user/edit-user.component';
// import { ChangePasswordComponent } from './change-password/change-password.component';
// import { SendVerifyEmailComponent } from './send-verify-email/send-verify-email.component';
// import { EditEmailComponent } from './edit-email/edit-email.component';
// import { RedirectComponent } from './redirect/redirect.component'
// import { WauErrorComponentComponent } from '../wau-error-component/wau-error-component.component'
// import { ErrorTranslatorPipe } from '../pipes/error-translator.pipe';
// import { WauSingInProviderComponent } from './wau-sing-in-provider/wau-sing-in-provider.component';
// import { SharedModule } from '../shared.module';
// import { AngularFirestoreModule } from 'angularfire2/firestore';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        // canActivate: [LoggedGuardService],
        component: LoginComponent
    },
    {
        path: 'signup',
        // canActivate: [LoggedGuardService],
        component: SignupComponent
    },
    // {
    //     path: 'login',
    //     canActivate: [LoggedGuardService],
    //     component: LoginComponent
    // },
    // {
    //     path: 'profile',
    //     canActivate: [AuthGuardService],
    //     component: ProfileComponent
    // },
    // {
    //     path: 'lost-password',
    //     component: LostPaswrodComponent
    // },
    // {
    //     path: 'redirect',
    //     component: RedirectComponent
    // }
    // ,
    // {
    // 	path: 'edit-user',
    // 	component: EditUserComponent
    // },

];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // SharedModule,
        // AngularFirestoreModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        // AuthPage,
        LoginComponent,
        SignupComponent,
        // ProfileComponent,
        // LostPaswrodComponent,
        // EditUserComponent,
        // ChangePasswordComponent,
        // SendVerifyEmailComponent,
        // EditEmailComponent,
        // RedirectComponent,
        // WauErrorComponentComponent,
        // ErrorTranslatorPipe,
        // WauSingInProviderComponent
    ]
})
export class AuthModule { }
