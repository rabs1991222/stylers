import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserCreate } from 'src/app/models/models';
import { Router } from '@angular/router';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

	user: UserCreate;
	loginError: string = '';

	constructor(
		private authService: AuthService,
		public router: Router,
	) { }

	ngOnInit() {
		this.user = {
			img: '',
			name: '',
			lastname: '',
			age: 0,
			gender: 'Female',
			email: '',
			password: ''
		} 
	}

	async signup() {
		try {
			await this.authService.signup(this.user);
			this.router.navigate(['']);
		} catch (error) {
			this.loginError = error.code;
		}
	}

	// errorProviderLogin(error): void {
	// 	this.loginError = error;
	// }

}
